# Project: SDL2Thing
# Makefile for Linux

CPP      = clang++
CC       = clang
OBJ      = main.o
LINKOBJ  = main.o
LIBS     = -lSDL2main -lSDL2
INCS     =
CXXINCS  =
BIN      = SDL2Thing
CXXFLAGS = $(CXXINCS) -g3
CFLAGS   = $(INCS) -g3
RM       = rm -f

.PHONY: all all-before all-after clean clean-custom

all: all-before $(BIN) all-after

clean: clean-custom
	${RM} $(OBJ) $(BIN)

$(BIN): $(OBJ)
	$(CPP) $(LINKOBJ) -o $(BIN) $(LIBS)

main.o: main.cpp
	$(CPP) -c src/main.cpp -o main.o $(CXXFLAGS)
