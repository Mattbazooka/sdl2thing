
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
//#include <fstream>
#include <iostream>

int factorial(int n){
  return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}

char* file_read(const char* filename) {
        SDL_RWops *rw = SDL_RWFromFile(filename, "rb");
        if (rw == NULL) return NULL;

        Sint64 res_size = SDL_RWsize(rw);
        char* res = new char[(int)res_size + 1];

        Sint64 nb_read_total = 0, nb_read = 1;
        char* buf = res;
        while (nb_read_total < res_size && nb_read != 0) {
                nb_read = SDL_RWread(rw, buf, 1, (res_size - nb_read_total));
                nb_read_total += nb_read;
                buf += nb_read;
        }
        SDL_RWclose(rw);
        if (nb_read_total != res_size) {
                delete[] res;
                return NULL;
        }

        res[nb_read_total] = '\0';
        return res;
}


int main(int argc, char *argv[]){

	//replace terminal with a file (file must exist?)
	//std::fstream fs;
	//fs.open("out2.txt", std::fstream::out | std::fstream::trunc);
	//std::streambuf* oldcout = std::cout.rdbuf(fs.rdbuf());


	std::cout << "start" << std::endl;

	//Init SDL2 with wanted parts
	SDL_Init(SDL_INIT_EVENTS|SDL_INIT_VIDEO|SDL_INIT_AUDIO|SDL_INIT_TIMER);

	std::cout << "init" << std::endl;

	//Create a window
	SDL_Window* window = SDL_CreateWindow("Window Name", SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,800,600,SDL_WINDOW_OPENGL);

	if (window == 0) {
		std::cout << "Error 1" << std::endl;
		return 1;
	}

	SDL_GLContext glcontext = SDL_GL_CreateContext(window);

	if (glcontext == 0) {
		std::cout << "Error 2" << std::endl;
		return 1;
	}

	//Get OpenGL functions
	//TODO move elseware
	//TODO check if functions are actually being set (SDL_GL_ExtensionSupported), idk how this works or if needed
	typedef void (APIENTRY * GL_ClearColor_Func)(float, float, float, float);
	GL_ClearColor_Func glClearColor = 0;
	glClearColor=(GL_ClearColor_Func) SDL_GL_GetProcAddress("glClearColor");

	typedef void (APIENTRY * GL_Clear_Func)(GLbitfield);
	GL_Clear_Func glClear = 0;
	glClear=(GL_Clear_Func) SDL_GL_GetProcAddress("glClear");

	typedef GLuint (APIENTRY * GL_CreateProgram_Func)(void);
	GL_CreateProgram_Func glCreateProgram = 0;
	glCreateProgram=(GL_CreateProgram_Func) SDL_GL_GetProcAddress("glCreateProgram");

	typedef GLuint (APIENTRY * GL_CreateShader_Func)(GLenum);
	GL_CreateShader_Func glCreateShader = 0;
	glCreateShader=(GL_CreateShader_Func) SDL_GL_GetProcAddress("glCreateShader");

	typedef void (APIENTRY * GL_ShaderSource_Func)(GLuint, GLsizei, const GLchar **, const GLint *);
	GL_ShaderSource_Func glShaderSource = 0;
	glShaderSource=(GL_ShaderSource_Func) SDL_GL_GetProcAddress("glShaderSource");

	typedef void (APIENTRY * GL_CompileShader_Func)(GLuint);
	GL_CompileShader_Func glCompileShader = 0;
	glCompileShader=(GL_CompileShader_Func) SDL_GL_GetProcAddress("glCompileShader");

	typedef void (APIENTRY * GL_AttachShader_Func)(GLuint, GLuint);
	GL_AttachShader_Func glAttachShader = 0;
	glAttachShader=(GL_AttachShader_Func) SDL_GL_GetProcAddress("glAttachShader");

	typedef void (APIENTRY * GL_LinkProgram_Func)(GLuint);
	GL_LinkProgram_Func glLinkProgram = 0;
	glLinkProgram=(GL_LinkProgram_Func) SDL_GL_GetProcAddress("glLinkProgram");

	typedef void (APIENTRY * GL_UseProgram_Func)(GLuint);
	GL_UseProgram_Func glUseProgram = 0;
	glUseProgram=(GL_UseProgram_Func) SDL_GL_GetProcAddress("glUseProgram");

	typedef void (APIENTRY * GL_GenBuffers_Func)(GLsizei, GLuint*);
	GL_GenBuffers_Func glGenBuffers = 0;
	glGenBuffers=(GL_GenBuffers_Func) SDL_GL_GetProcAddress("glGenBuffers");

	typedef void (APIENTRY * GL_BindBuffer_Func)(GLenum, GLuint);
	GL_BindBuffer_Func glBindBuffer = 0;
	glBindBuffer=(GL_BindBuffer_Func) SDL_GL_GetProcAddress("glBindBuffer");

	typedef const GLubyte * (APIENTRY * GL_GetString_Func)(GLenum);
	GL_GetString_Func glGetString = 0;
	glGetString=(GL_GetString_Func) SDL_GL_GetProcAddress("glGetString");

	typedef void (APIENTRY * GL_DepthFunc_Func)(GLenum);
	GL_DepthFunc_Func glDepthFunc = 0;
	glDepthFunc=(GL_DepthFunc_Func) SDL_GL_GetProcAddress("glDepthFunc");

	typedef void (APIENTRY * GL_Enable_Func)(GLenum);
	GL_Enable_Func glEnable = 0;
	glEnable=(GL_Enable_Func) SDL_GL_GetProcAddress("glEnable");

	typedef void (APIENTRY * GL_BufferData_Func)(GLenum, GLsizeiptr, const GLvoid *, GLenum);
	GL_BufferData_Func glBufferData = 0;
	glBufferData=(GL_BufferData_Func) SDL_GL_GetProcAddress("glBufferData");

	typedef void (APIENTRY * GL_GenVertexArrays_Func)(GLsizei, GLuint*);
	GL_GenVertexArrays_Func glGenVertexArrays = 0;
	glGenVertexArrays=(GL_GenVertexArrays_Func) SDL_GL_GetProcAddress("glGenVertexArrays");

	typedef void (APIENTRY * GL_BindVertexArray_Func)(GLuint);
	GL_BindVertexArray_Func glBindVertexArray = 0;
	glBindVertexArray=(GL_BindVertexArray_Func) SDL_GL_GetProcAddress("glBindVertexArray");

	typedef void (APIENTRY * GL_EnableVertexAttribArray_Func)(GLuint);
	GL_EnableVertexAttribArray_Func glEnableVertexAttribArray = 0;
	glEnableVertexAttribArray=(GL_EnableVertexAttribArray_Func) SDL_GL_GetProcAddress("glEnableVertexAttribArray");

	typedef void (APIENTRY * GL_VertexAttribPointer_Func)(GLuint, GLint, GLenum, GLboolean, GLsizei, const GLvoid*);
	GL_VertexAttribPointer_Func glVertexAttribPointer = 0;
	glVertexAttribPointer=(GL_VertexAttribPointer_Func) SDL_GL_GetProcAddress("glVertexAttribPointer");

	typedef void (APIENTRY * GL_DrawArrays_Func)(GLenum, GLint, GLsizei);
	GL_DrawArrays_Func glDrawArrays = 0;
	glDrawArrays=(GL_DrawArrays_Func) SDL_GL_GetProcAddress("glDrawArrays");

    typedef GLubyte* (APIENTRY * GL_GetStringi_Func)(GLenum, GLuint);
	GL_GetStringi_Func glGetStringi = 0;
	glGetStringi=(GL_GetStringi_Func) SDL_GL_GetProcAddress("glGetStringi");

    typedef GLenum (APIENTRY * GL_GetError_Func)(void);
	GL_GetError_Func glGetError = 0;
	glGetError=(GL_GetError_Func) SDL_GL_GetProcAddress("glGetError");

    typedef void (APIENTRY * GL_GetShaderiv_Func)(GLuint, GLenum, GLint*);
	GL_GetShaderiv_Func glGetShaderiv = 0;
	glGetShaderiv=(GL_GetShaderiv_Func) SDL_GL_GetProcAddress("glGetShaderiv");

    typedef void (APIENTRY * GL_GetShaderInfoLog_Func)(GLuint, GLsizei, GLsizei*, GLchar*);
	GL_GetShaderInfoLog_Func glGetShaderInfoLog = 0;
	glGetShaderInfoLog=(GL_GetShaderInfoLog_Func) SDL_GL_GetProcAddress("glGetShaderInfoLog");

    while(glGetError() != GL_NO_ERROR){
        std::cout << "GL ERROR AT POINT 1" << std::endl;
    }

/*
    //OpenGL info
	std::cout << "OpenGL functions loaded!" << std::endl;
	std::cout << glGetString(GL_VENDOR) << std::endl << glGetString(GL_RENDERER) << std::endl << glGetString(GL_VERSION) << std::endl << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    GLubyte* a;
    for(unsigned int i = 0; 1==1 ; ++i){
        if(i==0){
            std::cout << "in loop" << std::endl;
        }
        a = glGetStringi(GL_EXTENSIONS, i);
        if(a != 0){
            std::cout << a << std::endl;
        } else break;
    }
*/
	//data too draw
	float points[] = {
		-0.5f,  0.5f,  0.0f,
		0.5f, -0.5f,  0.0f,
		0.0f, -0.5f,  0.0f,
		0.5f,  0.5f,  0.0f,
		-0.5f, -0.5f,  0.0f,
		0.0f, -0.5f,  0.0f
		
	  
	};


	//create vertex buffer object
	GLuint vbo = 0;
	glGenBuffers (1, &vbo);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glBufferData (GL_ARRAY_BUFFER, 18 * sizeof (float), points, GL_STATIC_DRAW);

    while(glGetError() != GL_NO_ERROR){
        std::cout << "GL ERROR AT POINT 2" << std::endl;
    }

	//create vertex attribute object
	GLuint vao = 0;
	glGenVertexArrays (1, &vao);
	glBindVertexArray (vao);
	glEnableVertexAttribArray (0);
	glBindBuffer (GL_ARRAY_BUFFER, vbo);
	glVertexAttribPointer (0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    while(glGetError() != GL_NO_ERROR){
        std::cout << "GL ERROR AT POINT 3" << std::endl;
    }

	//create shader program

	//load shaders from files
	const char* vertexShaderBuffer = file_read("shaders/dot.vertex");
	const char* fragmentShaderBuffer = file_read("shaders/dot.fragment");

	const GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	const GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	glShaderSource(vertexShader, 1, &vertexShaderBuffer, NULL);
	glShaderSource(fragmentShader, 1, &fragmentShaderBuffer, NULL);

	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

    //check for errors
    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE){
        GLsizei len;
        glGetShaderiv(vertexShader, GL_SHADER_SOURCE_LENGTH, &len);
        GLchar* info = new GLchar[len+2];
        glGetShaderInfoLog(vertexShader, len+1, NULL, info);
        info[len+1] = '\0'; //probably dont need this
        std::cout << "Vertex shader compile failed!\nError: \n" << info << std::endl;
        delete[] info;
    }
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE){
        GLsizei len;
        glGetShaderiv(fragmentShader, GL_SHADER_SOURCE_LENGTH, &len);
        GLchar* info = new GLchar[len+2];
        glGetShaderInfoLog(fragmentShader, len+1, NULL, info);
        info[len+1] = '\0'; //probably dont need this
        std::cout << "Fragment shader compile failed!\nError: \n" << info << std::endl;
        delete[] info;
    }

	const GLuint program = glCreateProgram();

	if(program){
		glAttachShader(program, vertexShader);
		glAttachShader(program, fragmentShader);
		glLinkProgram(program);

	}

    while(glGetError() != GL_NO_ERROR){
        std::cout << "GL ERROR AT POINT 4" << std::endl;
    }



	std::cout << "loop!" << std::endl;
	//Main Loop
	SDL_Event event;
	bool keepAlive = true;
	while(keepAlive){
		//Handle events
		//TODO change to peek so it's non-blocking
		while (SDL_PollEvent(&event)) {
			switch(event.type){
				case SDL_QUIT:
				case SDL_KEYUP:
				{
					keepAlive = false;
					break;
				}
				default:{
					break;
				}
			}
		}
		//do some opengl stuff
		//TODO stop rendering as fast as possible

		//clear screen
		glClearColor(0.5f,0.0f,0.0f,0.5f);
		glClear(GL_COLOR_BUFFER_BIT);

		//draw
		glUseProgram(program);
		glBindVertexArray (vao);
		glDrawArrays (GL_TRIANGLES, 0, 6);

		//show
		SDL_GL_SwapWindow(window);

	}

    while(glGetError() != GL_NO_ERROR){
        std::cout << "GL ERROR AT POINT 5" << std::endl;
    }

	//Cleanup
	SDL_GL_DeleteContext(glcontext);

	SDL_DestroyWindow(window);

	SDL_Quit();

	//std::cout.rdbuf(oldcout);
	//fs.close();
}
